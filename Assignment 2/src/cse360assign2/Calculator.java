/*
 * 	Author: 		Luis D. Luvia
 * 	Class ID:		515
 * 	Assignment No.:	2
 */

/**
 * Calculator is a Java class designed to perform basic calculations,
 * such as addition, subtraction, multiplication, and division
 * of integers. It can also return a history of calculator
 * operations; internally, every time a new operation is
 * performed, the history of the calculator operations
 * is updated.
 * 
 * @author 		Luis D. Luvia
 * @version 	1.1
 * @since		1.0
 */

package cse360assign2;

public class Calculator {

	private int 	total;
	private String	history;
	
	/**
	 * The public method Calculator is the constructor for the
	 * Calculator class. It sets the private variables to their
	 * intended starting values.
	 * 
	 * @since		1.0
	 */
	public Calculator () {
		total = 0;  // not needed - included for clarity
		history = "0";
	}
	
	/**
	 * Returns the private integer variable total
	 * upon request from another method.
	 * 
	 * @return		the private integer <code>total</code>.
	 * @since		1.0
	 */
	public int getTotal () {
		return total;
	}
	
	/**
	 * Adds the given integer parameter to the <code>total</code>
	 * variable and calls updateHistory to add the addition
	 * operation and the integer parameter to the <code>history</code>
	 * variable.
	 * 
	 * @param value	the value to be added to <code>total</code>
	 * 
	 * @see			#updateHistory
	 * @since		1.0
	 */
	public void add (int value) {
		total += value;
		updateHistory(value, '+');
	}
	
	/**
	 * Subtracts the given integer parameter from the <code>total</code>
	 * variable and calls updateHistory to add the subtraction
	 * operation and the integer parameter to the <code>history</code>
	 * variable.
	 * 
	 * @param value	the value to be subtracted from <code>total</code>
	 * 
	 * @see			#updateHistory
	 * @since		1.0
	 */
	public void subtract (int value) {
		total -= value;
		updateHistory(value, '-');
	}
	
	/**
	 * Multiplies the given integer parameter with the <code>total</code>
	 * variable and calls updateHistory to add the multiplication
	 * operation and the integer parameter to the <code>history</code>
	 * variable.
	 * 
	 * @param value	the value to be multiplied with <code>total</code>
	 * 
	 * @see			#updateHistory
	 * @since		1.0
	 */
	public void multiply (int value) {
		total *= value;
		updateHistory(value, '*');
	}
	
	/**
	 * Divides <code>total</code> by the given integer parameter
	 * variable and calls updateHistory to add the division
	 * operation and the integer parameter to the <code>history</code>
	 * variable. If the given integer is zero, sets the <code>total</code>
	 * variable to zero instead.
	 * 
	 * @param value	the value to be divide <code>total</code> by
	 * 
	 * @see			#updateHistory
	 * @since		1.0
	 */
	public void divide (int value) {
		if (value != 0) {
			total /= value;
		} else {
			total = 0;
		}
		updateHistory(value, '/');
	}
	
	/**
	 * Returns the string associated with the variable <code>history</code>.
	 * 
	 * @return 		the private string variable <code>history</code>.
	 * @since		1.0
	 */
	public String getHistory () {
		return history;
	}
	
	/**
	 * After every operation, this method is called to add the operation
	 * and its operand to the private string variable <code>history</code>.
	 * 
	 * @param value 	the second operand that was used in the operation.
	 * @param operation	the operation performed on the two operands.
	 * @since		1.1
	 */
	private void updateHistory (int value, char operation) {
		history = history + " " + operation + " " + value;
	}
}
